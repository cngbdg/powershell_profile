$global:foregroundColor = 'white'
$global:backgroundColor = 'black'
$time = Get-Date
$psVersion = $host.Version.Major
$curUser = (Get-ChildItem Env:\USERNAME).Value
$curComp = (Get-ChildItem Env:\COMPUTERNAME).Value

# Configure Colors and title first

if ($host.UI.RawUI.WindowTitle -match "Administrator") {
  $host.UI.RawUI.WindowTitle="root@$curComp";
  $host.UI.RawUI.BackgroundColor = "DarkRed";
  $host.UI.RawUI.ForegroundColor = "white";
 }else{
  $host.UI.RawUI.BackgroundColor = "Black";
  $host.UI.RawUI.ForegroundColor = "white";
  $host.UI.RawUI.WindowTitle="$curUser@$curComp : $((Get-Location).Path)"
  Write-Host -NoNewLine ">" -foregroundColor Red
 }
 
 Function cdToPWS {
    set-location C:\PWS\
  }
 # Edit Pshell Profile
 Function Edit-Profile{
  notepad $profile
  }
  
  
 function Prompt {
  Write-Host -NoNewLine "cng" -foregroundColor $foregroundColor;
  
  Write-Host -NoNewLine "@$curComp" -foregroundColor Green;
  Write-Host -NoNewLine "[" -foregroundColor Yellow;
  Write-Host -NoNewLine "$((Get-Location).Path)" -foregroundColor $foregroundColor;
  Write-Host -NoNewLine "]" -foregroundColor Yellow;
  if ($host.UI.RawUI.WindowTitle -match "root") {
    $host.UI.RawUI.WindowTitle="root@$curComp : $((Get-Location).Path)";
    Write-Host -NoNewLine "#" -foregroundColor Yellow;
   }else{
    $host.UI.RawUI.WindowTitle = "$curUser@$curComp : $((Get-Location).Path)";
    Write-Host -NoNewLine ">" -foregroundColor Red
   }
   Return " "
}

function touch($file) {
	"" | Out-File $file -Encoding utf8
}

# http://stackoverflow.com/questions/39148304/fuser-equivalent-in-powershell/39148540#39148540
function fuser($relativeFile){
	$file = Resolve-Path $relativeFile
	echo "Looking for processes using $file"
	foreach ( $Process in (Get-Process)) {
		foreach ( $Module in $Process.Modules) {
			if ( $Module.FileName -like "$file*" ) {
				$Process | select id, path
			}
		}
	}
}

function killemacs{
  C:\tools\emacs\bin\emacsclientw.exe -e '(save-buffers-kill-emacs)'
}


Set-PSReadlineOption -BellStyle None # Bellsound
Set-PSRealineOption -
  Clear-Host

Push-Location
# Set-Location HKCU:\Console
# Set-Location "C:\Users\Charis-NicolasGeorgi"

Write-Host "Greetings, $curUser!" -foregroundColor $foregroundColor
Write-Host "It is: $($time.ToLongDateString())"
Write-Host "You're running PowerShell version: $psVersion" -foregroundColor Green
Write-Host "Your computer name is: $curComp" -foregroundColor Green
Write-Host "Happy scripting!" `n

Import-Module z
Import-Module posh-docker

set-alias pws cdToPWS
New-Alias -Name gh -Value Get-Help
New-Alias -Name ss -Value Select-String
New-Alias -Name new -Value New-Object
New-Alias -Name tp -Value Test-Path

# reload profile and shit
if($(get-alias -name which -ErrorAction "SilentlyContinue").count -lt 1){
  new-alias which get-command
 }
# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}
